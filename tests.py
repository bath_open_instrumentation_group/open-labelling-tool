import unittest

from config import Config
from openflexure_labelling_tool_app import create_app, db
from openflexure_labelling_tool_app.models import User


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite://"


class UserModelCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestConfig)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_password_hashing(self):
        u = User(username="alice")
        u.set_password("giraffe")
        self.assertFalse(u.check_password("rhino"))
        self.assertTrue(u.check_password("giraffe"))

    if __name__ == "__main__":
        unittest.main(verbosity=2)
