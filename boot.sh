#!/bin/bash
source venv/bin/activate
flask initdb
flask db migrate
flask db upgrade
exec gunicorn openflexure_labelling_tool:app -b :3000 --access-logfile - --error-logfile - --workers 3 --worker-class gevent --timeout 10000 
