import logging
import os
import random
import string
from pathlib import Path

from dotenv import load_dotenv

base_dir = Path(os.path.dirname(__file__))
load_dotenv(base_dir / ".env")


def gen_key(length: int):
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(length))


class Config(object):
    secret_key = os.environ.get("SECRET_KEY", default=None)
    if not secret_key:
        logging.warning(
            "No SECRET_KEY environment variable found. A key will be generated but sessions will not persist after application restarts."
        )
        secret_key = gen_key(32)

    data_dir =  base_dir / "instance" / "data"
    if not data_dir.exists():
        logging.warning("Making data directory.")
        data_dir.mkdir()

    SECRET_KEY = secret_key
    SQLALCHEMY_DATABASE_URI = (
        os.environ.get("DATABASE_URL") or f'sqlite:///{data_dir / "app.db"}'
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SLIDES_FOLDER = data_dir / "slides"
    ALLOWED_EXTENSIONS = ["png", "jpg", "jpeg", "gif", "bmp", "tiff"]
    APP_FOLDER = "openflexure_labelling_tool_app"
    LABEL_OPTIONS = [
        "Ring",
        "Trophozoite",
        "Schizont",
        "Gametocyte",
        "Dirt",
        "Other",
        "Nothing",
    ]
    IMAGES_PER_PAGE_IMAGE_LIST = 10
    ZSTACKS_PER_PAGE_ZSTACK_LIST = 10
    USERS_PER_PAGE_USER_LIST = 10
