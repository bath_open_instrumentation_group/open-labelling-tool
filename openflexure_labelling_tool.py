import logging
import sys

from openflexure_labelling_tool_app import create_app, db
from openflexure_labelling_tool_app.firstrun import setup
from openflexure_labelling_tool_app.models import (
    Image,
    Label,
    LabellingSession,
    Slide,
    User,
    ZMove,
    ZStack,
)

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

app = create_app()


@app.cli.command()
def initdb():
    logging.info("Creating tables...")
    db.create_all()
    with app.app_context():
        setup()


@app.cli.command()
def dropdb():
    db.drop_all()


@app.shell_context_processor
def make_shell_context():
    return {
        "db": db,
        "User": User,
        "Slide": Slide,
        "ZStack": ZStack,
        "Image": Image,
        "LabellingSession": LabellingSession,
        "ZMove": ZMove,
        "Label": Label,
    }
