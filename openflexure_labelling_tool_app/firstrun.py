import logging
import os

from .models import User, db


def add_admin(username, password):
    admin = User(username=username)
    admin.set_password(password)
    admin.make_admin()
    db.session.add(admin)
    db.session.commit()


def setup():

    logging.info("Checking if admin account exists...")
    admin_check = User.query.filter_by(username="admin").first()

    if not admin_check:
        logging.info("No existing administrator account found.")
        logging.info("Fetching target admin password from environment variable...")
        admin_pass = os.environ.get("ADMIN_PASSWORD", default=None)

        if admin_pass:
            logging.info("Valid admin password found.")
            logging.info("Creating new administrator account...")
            add_admin("admin", admin_pass)
        else:
            logging.warn(
                "No ADMIN_PASSWORD environment variable found. Unable to create admin account."
            )

    else:
        logging.info("Existing administrator account found. Leaving unchanged.")
