let totalZChanges = 0;
let fabricCanvas;
let slideImage, slideLabel, btnMoveUpZ, btnMoveDownZ, canvas;
//canvas things
let initialPos, bounds, rect = null, dragging = false;
const options = {
    newRectProps: {
        stroke: 'yellow',
        strokeWidth: 1,
        fill: '',
        hasControls: false,
        lockMovementX: true,
        lockMovementY: true,
        hoverCursor: "pointer"
    },
    savedRectProps: {
        stroke: 'red',
        strokeWidth: 1,
        fill: '',
        hasControls: false,
        lockMovementX: true,
        lockMovementY: true,
        hoverCursor: "pointer",
    }
}


//Updating functions

function changeSlideLabel() {
    "Z stack number " + zNumber;
}


function showNewImage() {
    fabric.Image.fromURL(zStack[zNumber].src, function (img) {
        fabricCanvas.setBackgroundImage(img, fabricCanvas.renderAll.bind(fabricCanvas), {
            scaleX: 1/scale,
            scaleY: 1/scale,
        });
    });
    document.getElementById('zStackProgress').value = zNumber;
    //changeSlideLabel();
}






function moveUpZ() {
    if (zNumber < zStack.length - 1) {
        zNumber++;
        totalZChanges++;
        showNewImage();
        saveZMove(true);
        if (zNumber == zStack.length - 1) {
            btnMoveUpZ.disabled = true;
        }
        else {
            btnMoveDownZ.disabled = false;
        }
    } else {

    }
}
function moveDownZ() {
    if (zNumber > 0) {
        zNumber--;
        totalZChanges++;
        showNewImage();
        saveZMove(false);
        if (zNumber == 0) {
            btnMoveDownZ.disabled = true;
        }
        else {
            btnMoveUpZ.disabled = false;
        }
    }
}

//canvas


function onMouseDown(e) {
    dragging = true;
    initialPos = {...e.pointer}
    bounds = {}

    rect = new fabric.Rect({
        left: initialPos.x,
        top: initialPos.y,
        width: 0, height: 0,
        labelOptions: defaultLabelOptions,
        askQuestion: false,
        ...options.newRectProps
    });
    fabricCanvas.add(rect)


}
function update(pointer) {
    if (initialPos.x > pointer.x) {
        bounds.x = Math.max(0, pointer.x)
        bounds.width = initialPos.x - bounds.x
    } else {
        bounds.x = initialPos.x
        bounds.width = pointer.x - initialPos.x
    }
    if (initialPos.y > pointer.y) {
        bounds.y = Math.max(0, pointer.y)
        bounds.height = initialPos.y - bounds.y
    } else {
        bounds.height = pointer.y - initialPos.y
        bounds.y = initialPos.y
    }
    rect.left = bounds.x
    rect.top = bounds.y
    rect.width = bounds.width
    rect.height = bounds.height
    rect.dirty = true
    fabricCanvas.requestRenderAllBound()
}
function onMouseMove(e) {
    if (!dragging) {
        return
    }
    requestAnimationFrame(() => update(e.pointer))
}
function onMouseUp(e) {

    if (dragging) {
        if (rect.width <= 5 || rect.height <= 5) {
            fabricCanvas.remove(rect)
        } else {
            fabricCanvas.setActiveObject(rect);
            rect.setCoords();
        }
        dragging = false;
    }
    if (fabricCanvas.getActiveObject()) {
        showLabelModal();
    }
}




function generateLabelModalRadioList() {
    var selectedLabel = fabricCanvas.getActiveObject();
    var currentLabelTypePosition = selectedLabel.labelOptions.indexOf(selectedLabel.labelType);
    if (selectedLabel.askQuestion) {
        questionString = "<p>" + selectedLabel.question + "</p>";
    } else {
        questionString = "";
    }
    radioListString = questionString + '<label><input class="uk-radio" type="radio" name="typeRadio" value = "' + selectedLabel.labelOptions[0] + '"';
    if (currentLabelTypePosition <= 0) {
        radioListString += ' checked';
    }
    radioListString = radioListString + '>' + selectedLabel.labelOptions[0] + '</label>&nbsp;&nbsp;';
    for (j = 1; j < selectedLabel.labelOptions.length; j++) {
        radioListString = radioListString + '<label><input class="uk-radio" type="radio" name="typeRadio" value = "' + selectedLabel.labelOptions[j] + '"';
        if (currentLabelTypePosition == j) {
            radioListString += ' checked';
        }
        radioListString = radioListString + '>' + selectedLabel.labelOptions[j] + '</label>&nbsp;&nbsp;';
    }
    return radioListString;
}

function showLabelModal() {
    var selectedLabel = fabricCanvas.getActiveObject();
    selectedLabel.stroke = 'blue';
    selectedLabel.dirty = true;
    fabricCanvas.renderAll();
    $("#labelTypeModalRadio").html(generateLabelModalRadioList());
    if (selectedLabel.askQuestion) {
        $("#btn_delete_label").disabled = true
    } else {
        $("#btn_delete_label").disabled = false
    }
    UIkit.modal(labelTypeModal).show();
};
