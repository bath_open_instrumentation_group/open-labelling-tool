import logging
import os
from logging.handlers import RotatingFileHandler

from flask import Flask, current_app, request
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect

from config import Config

db = SQLAlchemy()
migrate = Migrate()
login = LoginManager()
login.login_message_category = "warning"
login.login_view = "auth.login"
csrf = CSRFProtect()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    if not app.debug and not app.testing:

        if not os.path.exists("logs"):
            os.mkdir("logs")
        file_handler = RotatingFileHandler(
            "logs/openflexure-labelling-tool.log", maxBytes=10240, backupCount=10
        )
        file_handler.setFormatter(
            logging.Formatter(
                "%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]"
            )
        )
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

        app.logger.setLevel(logging.INFO)
        app.logger.info("OpenFlexure Labelling Tool startup")

    # Ensure the instance folder exists
    logging.info("Checking /instance directory...")
    try:
        logging.info("Creating /instance directory...")
        os.makedirs(app.instance_path)
    except OSError:
        pass

    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)

    csrf.init_app(app)

    from openflexure_labelling_tool_app.api import bp as api_bp

    app.register_blueprint(api_bp, url_prefix="/api")

    csrf.exempt(api_bp)

    from openflexure_labelling_tool_app.errors import bp as errors_bp

    app.register_blueprint(errors_bp)

    from openflexure_labelling_tool_app.auth import bp as auth_bp

    app.register_blueprint(auth_bp, url_prefix="/auth")

    from openflexure_labelling_tool_app.admin import bp as admin_bp

    app.register_blueprint(admin_bp, url_prefix="/admin")

    from openflexure_labelling_tool_app.main import bp as main_bp

    app.register_blueprint(main_bp)

    from openflexure_labelling_tool_app.slides import bp as slides_bp

    app.register_blueprint(slides_bp, url_prefix="/slides")

    return app


from openflexure_labelling_tool_app import models
