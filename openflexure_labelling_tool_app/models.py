import base64
import hashlib
import os
from datetime import datetime, timedelta
from pathlib import Path

from flask import current_app, url_for
from flask_login import UserMixin
from werkzeug.security import check_password_hash, generate_password_hash

from openflexure_labelling_tool_app import db, login


def same_as(column_name):
    def default_function(context):
        return context.current_parameters.get(column_name)

    return default_function


class PaginatedAPIMixin(object):
    @staticmethod
    def to_collection_dict(query, page, per_page, endpoint, **kwargs):
        resources = query.paginate(page, per_page, False)
        data = {
            "items": [item.to_dict() for item in resources.items],
            "_meta": {
                "page": page,
                "per_page": per_page,
                "total_pages": resources.pages,
                "total_items": resources.total,
            },
            "_links": {
                "self": url_for(endpoint, page=page, per_page=per_page, **kwargs),
                "next": url_for(endpoint, page=page + 1, per_page=per_page, **kwargs)
                if resources.has_next
                else None,
                "prev": url_for(endpoint, page=page - 1, per_page=per_page, **kwargs)
                if resources.has_prev
                else None,
            },
        }
        return data


class User(UserMixin, PaginatedAPIMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    experience = db.Column(db.String(64))
    is_admin = db.Column(db.Boolean)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    labelling_sessions = db.relationship(
        "LabellingSession", backref="user", lazy="dynamic"
    )
    z_moves = db.relationship("ZMove", backref="user", lazy="dynamic")
    labels = db.relationship("Label", backref="user", lazy="dynamic")

    @staticmethod
    def get_experience_levels():
        return [
            ("Beginner", "beginner"),
            ("Intermediate", "intermediate"),
            ("Expert", "expert"),
        ]

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User {}>".format(self.username)

    def to_dict(self):
        data = {
            "id": self.id,
            "username": self.username,
            "experience": self.experience,
            "is_admin": self.is_admin,
            "label_count": self.labels.count(),
            "labelling_session_count": self.labelling_sessions.count(),
            "z_move_count": self.z_moves.count(),
            "_links": {
                "self": url_for("api.get_user", id=self.id),
                "labelling_sessions": url_for(
                    "api.get_user_labelling_sessions", id=self.id
                ),
                "z_moves": url_for("api.get_user_z_moves", id=self.id),
                "labels": url_for("api.get_user_labels", id=self.id),
            },
        }
        return data

    def make_admin(self):
        self.is_admin = True

    def revoke_admin(self):
        self.is_admin = False
        self.revoke_token()

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode("utf-8")
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user

    def from_dict(self, data, new_user=False):
        for field in ["username", "experience"]:
            if field in data:
                setattr(self, field, data[field])
            if new_user and "password" in data:
                self.set_password(data["password"])


class Slide(PaginatedAPIMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sample_id = db.Column(db.String(4), index=True, unique=True)
    z_stacks = db.relationship("ZStack", backref="slide", lazy="dynamic")

    def __repr__(self):
        return "<Slide {}>".format(self.sample_id)

    def to_dict(self):
        data = {
            "id": self.id,
            "z_stacks_count": self.z_stacks.count(),
            "_links": {
                "self": url_for("api.get_slide", id=self.id),
                "z-stacks": url_for("api.get_z_stacks", id=self.id),
            },
        }
        return data


class ZStack(PaginatedAPIMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    slide_id = db.Column(db.Integer, db.ForeignKey("slide.id"))
    name = db.Column(db.String, default=same_as("id"))
    is_active = db.Column(db.Boolean, default=True)
    images = db.relationship("Image", backref="z_stack", lazy="dynamic")
    labelling_sessions = db.relationship(
        "LabellingSession", backref="z_stack", lazy="dynamic"
    )

    def __repr__(self):
        return f"<Z Stack {self.id} ({self.name})>"

    def to_dict(self):
        data = {
            "id": self.id,
            "slide_id": self.slide_id,
            "name": self.name,
            "is_active": self.is_active,
            "images_count": self.images.count(),
            "labelling_sessions_count": self.labelling_sessions.count(),
            "_links": {
                "self": url_for("api.get_z_stack", id=self.id),
                "images": url_for("api.get_z_stack_images", id=self.id),
                "labelling_sessions": url_for(
                    "api.get_z_stack_labelling_sessions", id=self.id
                ),
            },
        }
        return data


class Image(PaginatedAPIMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    z_stack_id = db.Column(db.Integer, db.ForeignKey("z_stack.id"))
    slide_x = db.Column(db.Integer)
    slide_y = db.Column(db.Integer)
    slide_z = db.Column(db.Integer)
    file_location = db.Column(db.String, unique=True)
    hash = db.Column(db.String(256))
    z_moves = db.relationship("ZMove", backref="to_image", lazy="dynamic")
    labels = db.relationship("Label", backref="in_image", lazy="dynamic")

    def __repr__(self):
        return "<Image {} stored at {}>".format(self.id, self.file_location)

    def get_image_location(self):
        image_location = Path(current_app.config["SLIDES_FOLDER"]) / self.file_location
        if image_location.exists():
            return image_location

    def set_hash(self):
        image_location = self.get_image_location()
        with open(image_location, "rb") as f:
            image_binary = f.read()
            self.hash = hashlib.sha256(image_binary).digest()
            return
        return

    def check_hash(self):
        image_location = self.get_image_location()
        with open(image_location, "rb") as f:
            image_binary = f.read()
            return self.hash == hashlib.sha256(image_binary).digest()
        return

    def to_dict(self):
        data = {
            "id": self.id,
            "z_stack_id": self.z_stack_id,
            "slide_x": self.slide_x,
            "slide_y": self.slide_y,
            "slide_z": self.slide_z,
            "file_location": self.file_location,
            "check_hash": self.check_hash(),
            "labels_count": self.labels.count(),
            "_links": {
                "self": url_for("api.get_image", id=self.id),
                "labels": url_for("api.get_image_labels", id=self.id),
            },
        }
        return data


class LabellingSession(PaginatedAPIMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    z_stack_id = db.Column(db.Integer, db.ForeignKey("z_stack.id"))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    time_taken = db.Column(db.Interval, default=timedelta())
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    is_completed = db.Column(db.Boolean)
    z_moves = db.relationship("ZMove", backref="labelling_session", lazy="dynamic")
    labels = db.relationship("Label", backref="labelling_session", lazy="dynamic")

    def __repr__(self):
        return "<Labelling Session {}>".format(self.id)

    def to_dict(self):
        data = {
            "id": self.id,
            "z_stack_id": self.z_stack_id,
            "user_id": self.user_id,
            "time_taken": str(self.time_taken),
            "timestamp": self.timestamp.isoformat() + "Z",
            "is_completed": self.is_completed,
            "labels_counts": self.labels.count(),
            "links": {
                "self": url_for("api.get_labelling_session", id=self.id),
                "labels": url_for("api.get_labelling_session_labels", id=self.id),
                "images": url_for("api.get_labelling_session_images", id=self.id),
            },
        }
        return data

    def from_dict(self, data, new_labelling_session=False):
        for field in ["z_stack_id", "user_id"]:
            if field in data:
                setattr(self, field, data[field])
        if new_labelling_session == True:
            self.is_completed = False


class ZMove(PaginatedAPIMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image_id = db.Column(db.Integer, db.ForeignKey("image.id"))
    labelling_session_id = db.Column(db.Integer, db.ForeignKey("labelling_session.id"))
    is_move_up = db.Column(db.Boolean())
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def __repr__(self):
        if self.is_move_up:
            direction = "Up"
        else:
            direction = "Down"
        return "<Z Move {} to Image ID {}>".format(direction, self.image_id)

    def to_dict(self):
        data = {
            "id": self.id,
            "image_id": self.image_id,
            "labelling_session_id": self.labelling_session_id,
            "is_move_up": self.is_move_up,
            "timestamp": self.timestamp.isoformat() + "Z",
            "_links": {"self": url_for("api.get_z_move", id=self.id)},
        }
        return data


class Label(PaginatedAPIMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image_id = db.Column(db.Integer, db.ForeignKey("image.id"))
    labelling_session_id = db.Column(db.Integer, db.ForeignKey("labelling_session.id"))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    shape = db.Column(db.String(32), default="rect")
    image_x = db.Column(db.Integer)
    image_y = db.Column(db.Integer)
    image_delta_x = db.Column(db.Integer)
    image_delta_y = db.Column(db.Integer)
    label_type = db.Column(db.Text, index=True)
    is_deleted = db.Column(db.Boolean, default=False)
    ask_question = db.Column(db.Boolean, default=False)
    question = db.Column(db.Text)
    label_options = db.Column(db.PickleType)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return "<Label {}>".format(self.id)

    def to_dict(self):
        data = {
            "id": self.id,
            "image_id": self.image_id,
            "user_id": self.user_id,
            "shape": self.shape,
            "image_x": self.image_x,
            "image_y": self.image_y,
            "image_delta_x": self.image_delta_x,
            "image_delta_y": self.image_delta_y,
            "label_type": self.label_type,
            "is_deleted": self.is_deleted,
            "ask_question": self.ask_question,
            "question": self.question,
            "label_options": self.label_options,
            "timestamp": self.timestamp.isoformat() + "Z",
            "labelling_session_id": self.labelling_session_id,
            "_links": {
                "self": url_for("api.get_label", id=self.id),
            },
        }
        return data

    def from_dict(self, data):
        for field in [
            "image_id",
            "user_id",
            "shape",
            "image_x",
            "image_y",
            "image_delta_x",
            "image_delta_y",
            "label_type",
            "is_deleted",
            "ask_question",
            "question",
            "label_options",
            "timestamp",
            "labelling_session_id",
        ]:
            if field in data:
                setattr(self, field, data[field])


@login.user_loader
def load_user(id):
    return User.query.get(int(id))
