from flask import render_template, request

from openflexure_labelling_tool_app import db
from openflexure_labelling_tool_app.api.errors import error_response as api_error_response
from openflexure_labelling_tool_app.errors import bp


def wants_json_response():
    return (
        request.accept_mimetypes["application/json"]
        >= request.accept_mimetypes["text/html"]
    )


@bp.app_errorhandler(404)
def not_found_error(error):
    if wants_json_response():
        return api_error_response(404)
    return render_template("errors/404.html", title="404: Page not found"), 404


@bp.app_errorhandler(500)
def internal_error(error):
    db.session.rollback()
    if wants_json_response():
        return api_error_response(500)
    return (
        render_template("errors/500.html", title="An unexpected error has occured"),
        500,
    )


@bp.app_errorhandler(405)
def method_error(error):
    if wants_json_response():
        return api_error_response(405)
    return render_template("errors/405.html", title="405: Method not allowed"), 405
