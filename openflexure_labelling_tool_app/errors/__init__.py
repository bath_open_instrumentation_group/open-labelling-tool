from flask import Blueprint

bp = Blueprint("errors", __name__)

from openflexure_labelling_tool_app.errors import handlers
