from flask_login import current_user
from flask_wtf import FlaskForm
from wtforms import BooleanField, PasswordField, RadioField, StringField, SubmitField
from wtforms.validators import DataRequired, EqualTo, ValidationError

from openflexure_labelling_tool_app.models import User


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember_me = BooleanField("Remember Me")
    submit = SubmitField("Sign In")


class RegistrationForm(FlaskForm):

    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    password2 = PasswordField(
        "Repeat Password", validators=[DataRequired(), EqualTo("password")]
    )
    experience = RadioField(
        "Your experience level",
        choices=User.get_experience_levels(),
        validators=[DataRequired()],
    )
    submit = SubmitField("Register")

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError("Please use a different username.")


class ResetPasswordForm(FlaskForm):

    current_password = PasswordField("Current password", validators=[DataRequired()])
    password = PasswordField("New password", validators=[DataRequired()])
    password2 = PasswordField(
        "Repeat new password", validators=[DataRequired(), EqualTo("password")]
    )
    submit = SubmitField("Reset Password")

    def validate_current_password(self, current_password):
        if not current_user.check_password(current_password.data):
            raise ValidationError("Your current password is not correct.")
