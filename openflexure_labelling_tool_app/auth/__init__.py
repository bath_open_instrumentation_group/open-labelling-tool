from flask import Blueprint

bp = Blueprint("auth", __name__)

from openflexure_labelling_tool_app.auth import routes
