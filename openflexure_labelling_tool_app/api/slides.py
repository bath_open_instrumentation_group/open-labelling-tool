from flask import jsonify, request

from openflexure_labelling_tool_app import db
from openflexure_labelling_tool_app.api import bp
from openflexure_labelling_tool_app.api.auth import token_auth
from openflexure_labelling_tool_app.api.errors import bad_request
from openflexure_labelling_tool_app.models import Slide


@bp.route("/slides", methods=["GET"])
@token_auth.login_required
def get_slides():
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = Slide.to_collection_dict(Slide.query, page, per_page, "api.get_slides")
    return jsonify(data)


@bp.route("/slides/<int:id>", methods=["GET"])
@token_auth.login_required
def get_slide(id):
    return jsonify(Slide.query.get_or_404(id).to_dict())


@bp.route("slides/<int:id>/z-stacks", methods=["GET"])
@token_auth.login_required
def get_slide_labelling_sessions(id):
    slide = Slide.query.get_or_404(id)
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = Slide.to_collection_dict(
        slide.z - stacks, page, per_page, "api.get_labelling_session", id=id
    )
    return jsonify(data)
