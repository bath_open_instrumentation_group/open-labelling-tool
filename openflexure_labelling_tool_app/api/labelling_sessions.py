import json

from flask import jsonify, request, url_for

from openflexure_labelling_tool_app import db
from openflexure_labelling_tool_app.api import bp
from openflexure_labelling_tool_app.api.auth import token_auth
from openflexure_labelling_tool_app.api.errors import bad_request
from openflexure_labelling_tool_app.models import LabellingSession


@bp.route("/labelling-sessions", methods=["GET"])
@token_auth.login_required
def get_labelling_sessions():
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = LabellingSession.to_collection_dict(
        LabellingSession.query, page, per_page, "api.get_labelling_sessions"
    )
    return jsonify(data)


@bp.route("/labelling-sessions/<int:id>", methods=["GET"])
@token_auth.login_required
def get_labelling_session(id):
    return jsonify(LabellingSession.query.get_or_404(id).to_dict())


@bp.route("/labelling-sessions/<int:id>", methods=["PUT"])
@token_auth.login_required
def update_labelling_session(id):
    labelling_session = LabellingSession.query.get_or_404(id)
    data = request.get_json() or {}
    labelling_session.from_dict(data, new_labelling_session=False)
    db.session.commit()
    return jsonify(labelling_session.to_dict())


@bp.route("/labelling-sessions", methods=["POST"])
@token_auth.login_required
def create_labelling_session():
    data = request.get_json() or {}
    if "z_stack_id" not in data or "user_id" not in data:
        return bad_request("Must include z_stack_id and user_id fields.")
    labelling_session = LabellingSession()
    labelling_session.from_dict(data, new_labelling_session=True)
    db.session.add(labelling_session)
    db.session.commit()
    response = jsonify(labelling_session.to_dict())
    response.status_code = 201
    response.headers["Location"] = url_for(
        "api.get_labelling_session", id=labelling_session.id
    )
    return response


@bp.route("labelling-sessions/<int:id>/labels", methods=["GET"])
@token_auth.login_required
def get_labelling_session_labels(id):
    labelling_session = LabellingSession.query.get_or_404(id)
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = LabellingSession.to_collection_dict(
        labelling_session.labels,
        page,
        per_page,
        "api.get_labelling_session_labels",
        id=id,
    )
    return jsonify(data)
