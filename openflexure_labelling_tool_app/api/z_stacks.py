from flask import jsonify, request

from openflexure_labelling_tool_app import db
from openflexure_labelling_tool_app.api import bp
from openflexure_labelling_tool_app.api.auth import token_auth
from openflexure_labelling_tool_app.api.errors import bad_request
from openflexure_labelling_tool_app.models import LabellingSession, ZStack


@bp.route("/z-stacks", methods=["GET"])
@token_auth.login_required
def get_z_stacks():
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = ZStack.to_collection_dict(ZStack.query, page, per_page, "api.get_z_stacks")
    return jsonify(data)


@bp.route("/z-stacks/<int:id>", methods=["GET"])
@token_auth.login_required
def get_z_stack(id):
    return jsonify(ZStack.query.get_or_404(id).to_dict())


@bp.route("z-stacks/<int:id>/labelling-sessions", methods=["GET"])
@token_auth.login_required
def get_z_stack_labelling_sessions(id):
    z_stack = ZStack.query.get_or_404(id)
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = ZStack.to_collection_dict(
        z_stack.labelling_sessions, page, per_page, "api.get_labelling_session", id=id
    )
    return jsonify(data)


@bp.route("z-stacks/<int:id>/images", methods=["GET"])
@token_auth.login_required
def get_z_stack_images(id):
    z_stack = ZStack.query.get_or_404(id)
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = ZStack.to_collection_dict(
        z_stack.images, page, per_page, "api.get_z_stack_images", id=id
    )
    return jsonify(data)


@bp.route("labelling-sessions/<int:id>/images", methods=["GET"])
@token_auth.login_required
def get_labelling_session_images(id):
    labelling_session = LabellingSession.query.get_or_404(id)
    z_stack = ZStack.query.get_or_404(labelling_session.z_stack_id)
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = ZStack.to_collection_dict(
        z_stack.images, page, per_page, "api.get_z_stack_images", id=id
    )
    return jsonify(data)
