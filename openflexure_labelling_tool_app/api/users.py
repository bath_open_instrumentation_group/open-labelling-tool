from flask import jsonify, request, url_for

from openflexure_labelling_tool_app import db
from openflexure_labelling_tool_app.api import bp
from openflexure_labelling_tool_app.api.auth import token_auth
from openflexure_labelling_tool_app.api.errors import bad_request
from openflexure_labelling_tool_app.models import User


@bp.route("/users/<int:id>", methods=["GET"])
@token_auth.login_required
def get_user(id):
    return jsonify(User.query.get_or_404(id).to_dict())


@bp.route("/users", methods=["GET"])
@token_auth.login_required
def get_users():
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = User.to_collection_dict(User.query, page, per_page, "api.get_users")
    return jsonify(data)


@bp.route("/users", methods=["POST"])
@token_auth.login_required
def create_user():
    data = request.get_json() or {}
    if "username" not in data or "password" not in data or "experience" not in data:
        return bad_request("Must include username, experience and password fields.")
    if User.query.filter_by(username=data["username"]).first():
        return bad_request("Please use a different username.")

    # TODO check experience is in the valid options, or AI
    user = User()
    user.from_dict(data, new_user=True)
    user.is_admin = False
    db.session.add(user)
    db.session.commit()
    response = jsonify(user.to_dict())
    response.status_code = 201
    response.headers["Location"] = url_for("api.get_user", id=user.id)
    return response


@bp.route("/users/<int:id>", methods=["PUT"])
@token_auth.login_required
def update_user(id):
    user = User.query.get_or_404(id)
    data = request.get_json() or {}
    if (
        "username" in data
        and data["username"] != user.username
        and User.query.filter_by(username=data["username"]).first()
    ):
        return bad_request("Please use a different username.")
    user.from_dict(data, new_user=False)
    db.session.commit()
    return jsonify(user.to_dict())


@bp.route("/users/<int:id>/labels", methods=["GET"])
@token_auth.login_required
def get_user_labels(id):
    user = User.query.get_or_404(id)
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = User.to_collection_dict(
        user.labels, page, per_page, "api.get_user_labels", id=id
    )
    return jsonify(data)


@bp.route("/users/<int:id>/z-moves", methods=["GET"])
@token_auth.login_required
def get_user_z_moves(id):
    user = User.query.get_or_404(id)
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = User.to_collection_dict(
        user.z_moves, page, per_page, "api.get_user_z_moves", id=id
    )
    return jsonify(data)


@bp.route("/users/<int:id>/labelling-sessions", methods=["GET"])
@token_auth.login_required
def get_user_labelling_sessions(id):
    user = User.query.get_or_404(id)
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = User.to_collection_dict(
        user.labelling_sessions,
        page,
        per_page,
        "api.get_user_labelling_sessions",
        id=id,
    )
    return jsonify(data)
