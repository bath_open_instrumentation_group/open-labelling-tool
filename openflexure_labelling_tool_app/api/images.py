from flask import jsonify, request, url_for

from openflexure_labelling_tool_app.api import bp
from openflexure_labelling_tool_app.api.auth import token_auth
from openflexure_labelling_tool_app.models import Image


@bp.route("/images/<int:id>", methods=["GET"])
@token_auth.login_required
def get_image(id):
    return jsonify(Image.query.get_or_404(id).to_dict())


@bp.route("/images", methods=["GET"])
@token_auth.login_required
def get_images():
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = Image.to_collection_dict(Image.query, page, per_page, "api.get_images")
    return jsonify(data)


@bp.route("images/<int:id>/labels", methods=["GET"])
@token_auth.login_required
def get_image_labels(id):
    image = Image.query.get_or_404(id)
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = Image.to_collection_dict(
        image.labels, page, per_page, "api.get_labels", id=id
    )
    return jsonify(data)
