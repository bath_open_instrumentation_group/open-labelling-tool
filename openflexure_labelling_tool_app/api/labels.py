from flask import jsonify, request, url_for

from openflexure_labelling_tool_app import db
from openflexure_labelling_tool_app.api import bp, labelling_sessions
from openflexure_labelling_tool_app.api.auth import token_auth
from openflexure_labelling_tool_app.api.errors import bad_request
from openflexure_labelling_tool_app.models import Label


@bp.route("/labels/<int:id>", methods=["GET"])
@token_auth.login_required
def get_label(id):
    return jsonify(Label.query.get_or_404(id).to_dict())


@bp.route("/labels", methods=["GET"])
@token_auth.login_required
def get_labels():
    page = request.args.get("page", 1, type=int)
    per_page = min(request.args.get("per_page", 10, type=int), 100)
    data = Label.to_collection_dict(
        Label.query, page, per_page, "api.get_labelling_sessions"
    )
    return jsonify(data)


@bp.route("/labels", methods=["POST"])
@token_auth.login_required
def create_label():
    data = request.get_json() or {}
    if (
        "image_id" not in data
        or "user_id" not in data
        or "labelling_session_id" not in data
    ):
        return bad_request("Must include z_stack_id and user_id fields.")
    label = Label()
    label.from_dict(data)
    db.session.add(label)
    db.session.commit()
    response = jsonify(label.to_dict())
    response.status_code = 201
    response.headers["Location"] = url_for("api.get_label", id=label.id)
    return response
