from flask import Blueprint

bp = Blueprint("slides", __name__)

from openflexure_labelling_tool_app.slides import routes
