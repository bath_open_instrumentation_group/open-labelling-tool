import json
import logging
from datetime import datetime
from pathlib import Path

from flask import (
    abort,
    current_app,
    flash,
    jsonify,
    redirect,
    render_template,
    request,
    safe_join,
    send_file,
    send_from_directory,
    url_for,
)
from flask_login import current_user, login_required
from sqlalchemy.sql.expression import func

from openflexure_labelling_tool_app import db
from openflexure_labelling_tool_app.models import (
    Image,
    Label,
    LabellingSession,
    User,
    ZMove,
    ZStack,
)
from openflexure_labelling_tool_app.slides import bp


@bp.route("/", methods=["GET"])
@login_required
def slides():
    # get last labelling session that hasn't been completed
    labelling_session = (
        LabellingSession.query.filter_by(user_id=current_user.id)
        .filter_by(is_completed=False)
        .order_by(LabellingSession.timestamp)
        .first()
    )
    # create new labelling session from user, and next in list of images
    if labelling_session is None:
        z_stack = get_next_z_stack(current_user)
        if z_stack is None:
            flash("There are no more slides for you to label!", "success")
            return redirect(url_for("main.index"))
        labelling_session = LabellingSession(
            z_stack_id=z_stack.id, user_id=current_user.id, is_completed=False
        )
        db.session.add(labelling_session)
        db.session.commit()
    labelling_session.time_stamp = datetime.utcnow()
    db.session.commit()
    return render_template(
        "slides/slides.html",
        title="Label slides",
        labels=labelling_session.labels.filter_by(is_deleted=False).all(),
        zStack=labelling_session.z_stack,
        images=labelling_session.z_stack.images.order_by(Image.slide_z.asc()).all(),
        labelling_session=labelling_session,
        slides_folder=current_app.config["SLIDES_FOLDER"],
        default_label_options=current_app.config["LABEL_OPTIONS"],
    )


@bp.route("/img/<int:id>", methods=["GET"])
@login_required
def get_img(id):
    image = Image.query.get_or_404(id)
    if not image.check_hash():
        return abort(404)
    return send_from_directory(current_app.config["SLIDES_FOLDER"], image.file_location)


@bp.route("/save-label", methods=["POST"])
@login_required
def save_label():
    label_id = request.form["label_id"]
    ask_question = json.loads(request.form["ask_question"])
    labelling_session_id = int(request.form["labelling_session_id"])
    shape = request.form["shape"]
    image_x = int(request.form["image_x"])
    image_y = int(request.form["image_y"])
    image_delta_x = int(request.form["image_delta_x"])
    image_delta_y = int(request.form["image_delta_y"])
    label_type = request.form["label_type"]
    image_id = int(request.form["image_id"])
    label_options = request.form["label_options"].split(",")
    if ask_question == False:
        label = Label(
            image_id=image_id,
            labelling_session_id=labelling_session_id,
            user_id=current_user.id,
            shape=shape,
            image_x=image_x,
            image_y=image_y,
            image_delta_x=image_delta_x,
            image_delta_y=image_delta_y,
            label_type=label_type,
            label_options=label_options,
            is_deleted=False,
            timestamp=datetime.utcnow(),
        )
        labelling_session = LabellingSession.query.get(labelling_session_id)
        labelling_session.labels.append(label)
    else:
        label = Label.query.get(label_id)
        label.label_type = label_type
        label.timestamp = datetime.utcnow()
    db.session.commit()
    return str(label.id)


@bp.route("/save-z-move", methods=["POST"])
@login_required
def save_z_move():
    image_id = int(request.form["image_id"])
    labelling_session_id = int(request.form["labelling_session_id"])
    is_move_up = request.form["is_move_up"] == "true"
    z_move = ZMove(
        image_id=image_id,
        labelling_session_id=labelling_session_id,
        is_move_up=is_move_up,
        timestamp=datetime.utcnow(),
        user_id=current_user.id,
    )
    db.session.add(z_move)
    db.session.commit()
    return jsonify({"message": "z move saved"})


@bp.route("/save-labelling-session", methods=["POST"])
@login_required
def save_labelling_session():
    labelling_session_id = int(request.form["labelling_session_id"])
    is_completed = json.loads(request.form["is_completed"])
    labelling_session = LabellingSession.query.get(int(labelling_session_id))
    if current_user.id == labelling_session.user_id:
        labelling_session.time_taken = (
            labelling_session.time_taken
            + datetime.utcnow()
            - labelling_session.timestamp
        )
        labelling_session.is_completed = is_completed
        db.session.commit()
    return jsonify({"message": "labelling session saved"})


@bp.route("/delete-label", methods=["POST"])
@login_required
def delete_label():
    label_id = int(request.form["label_id"])
    label = Label.query.get(label_id)
    labelling_session_id = int(request.form["labelling_session_id"])
    if label.labelling_session_id == labelling_session_id:
        if label.user_id == current_user.id or current_user.is_admin:
            label.is_deleted = True
            db.session.commit()
            return jsonify({"message": "label deleted"})
        else:
            return jsonify({"message": "You do not have permission to do that"}), 403
    else:
        return jsonify({"message": "Bad request"}), 400


def get_next_z_stack(current_user):
    # get the first zstack where there are no labelling sessions by the current user.
    z_stacks = (
        ZStack.query.filter_by(is_active=True)
        .outerjoin(ZStack.labelling_sessions)
        .group_by(ZStack.id)
        .order_by(func.count(LabellingSession.id).asc())
        .all()
    )
    for z_stack in z_stacks:
        if (
            z_stack.labelling_sessions.filter(
                LabellingSession.user_id == current_user.id
            ).count()
            == 0
        ):
            return z_stack
    return None
