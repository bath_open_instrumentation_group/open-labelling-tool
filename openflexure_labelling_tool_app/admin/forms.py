from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileField, FileRequired
from wtforms import FileField, IntegerField, SelectField, StringField, SubmitField
from wtforms.validators import DataRequired, InputRequired, Length, ValidationError

from openflexure_labelling_tool_app.models import Slide, ZStack


class EmptyForm(FlaskForm):
    submit = SubmitField("Submit")


class AddSlideForm(FlaskForm):
    sample_id = StringField("Sample ID", validators=[DataRequired(), Length(max=4)])
    submit = SubmitField("Add sample")

    def validate_sample_id(self, sample_id):
        slide = Slide.query.filter_by(sample_id=sample_id.data).first()
        if slide is not None:
            raise ValidationError("This sample ID is already recorded.")


class AddImagesJSONForm(FlaskForm):
    json_file = FileField(
        "JSON File",
        validators=[FileRequired(), FileAllowed(["json"], "Only JSON files allowed.")],
    )
    submit = SubmitField("Load images from JSON")


class AddZStackForm(FlaskForm):
    sample_id = SelectField("Sample ID")
    name = StringField("Name")
    submit = SubmitField("Add Z-stack")


class AddImageForm(FlaskForm):
    sample_id = SelectField("Sample ID", validate_choice=False)
    z_stack_id = SelectField("Z-stack ID", validate_choice=False)
    slide_x = IntegerField("x position", validators=[InputRequired()])
    slide_y = IntegerField("y position", validators=[InputRequired()])
    slide_z = IntegerField("z position", validators=[InputRequired()])
    image_file = FileField(
        "Image file",
        validators=[
            FileRequired(),
            FileAllowed(["png", "jpg", "jpeg", "gif", "bmp", "tiff"], "Only images allowed."),
        ],
    )
    submit = SubmitField("Add image")
