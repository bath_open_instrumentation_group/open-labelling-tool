from flask import Blueprint

bp = Blueprint("admin", __name__)

from openflexure_labelling_tool_app.admin import routes
