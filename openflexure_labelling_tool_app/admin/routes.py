import json
import logging
import os
from functools import wraps
from pathlib import Path

from flask import (
    current_app,
    flash,
    jsonify,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_login import current_user, login_required
from werkzeug.urls import url_parse
from werkzeug.utils import secure_filename

from openflexure_labelling_tool_app import db
from openflexure_labelling_tool_app.admin import bp
from openflexure_labelling_tool_app.admin.forms import (
    AddImageForm,
    AddImagesJSONForm,
    AddSlideForm,
    AddZStackForm,
    EmptyForm,
)
from openflexure_labelling_tool_app.models import Image, Slide, User, ZStack


def admin_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user.is_admin:
            flash("You need to be an admin to view this page.", "error")
            return redirect(url_for("main.index"))
        return func(*args, **kwargs)

    return decorated_view


@bp.route("/dashboard", methods=["GET"])
@bp.route("/")
@login_required
@admin_required
def dashboard():
    return render_template("admin/dashboard.html", title="Admin: Dashboard")


@bp.route("/users", methods=["GET"])
@login_required
@admin_required
def users():
    form = EmptyForm()
    page = request.args.get("page", 1, type=int)
    users = User.query.paginate(
        page, current_app.config["USERS_PER_PAGE_USER_LIST"], False
    )
    next_url = url_for("admin.users", page=users.next_num) if users.has_next else None
    prev_url = url_for("admin.users", page=users.prev_num) if users.has_prev else None
    return render_template(
        "admin/users.html", users=users, title="Admin: user list", form=form
    )


@bp.route("/z-stacks", methods=["GET"])
@login_required
@admin_required
def z_stacks():
    form = EmptyForm()
    page = request.args.get("page", 1, type=int)
    z_stacks = ZStack.query.paginate(
        page, current_app.config["ZSTACKS_PER_PAGE_ZSTACK_LIST"], False
    )
    next_url = (
        url_for("admin.z_stacks", page=z_stacks.next_num) if z_stacks.has_next else None
    )
    prev_url = (
        url_for("admin.z_stacks", page=z_stacks.prev_num) if z_stacks.has_prev else None
    )
    return render_template(
        "admin/z-stacks.html", z_stacks=z_stacks, title="Admin: z-stack list", form=form
    )


@bp.route("/images", methods=["GET"])
@login_required
@admin_required
def images():
    page = request.args.get("page", 1, type=int)
    images = Image.query.paginate(
        page, current_app.config["IMAGES_PER_PAGE_IMAGE_LIST"], False
    )
    next_url = (
        url_for("admin.images", page=images.next_num) if images.has_next else None
    )
    prev_url = (
        url_for("admin.images", page=images.prev_num) if images.has_prev else None
    )
    return render_template(
        "admin/images.html",
        images=images,
        title="Admin: image list",
        slides_folder=current_app.config["SLIDES_FOLDER"],
        label_options=current_app.config["LABEL_OPTIONS"],
    )


def get_or_create(session, model, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance
    else:
        instance = model(**kwargs)
        session.add(instance)
        db.session.commit()
        return instance


def allowed_file(filename):
    return (
        "." in filename
        and filename.rsplit(".", 1)[1].lower()
        in current_app.config["ALLOWED_EXTENSIONS"]
    )


def load_images_from_json(json_file):
    new_images = json.loads(json_file)
    success = True
    failed_images = []
    for slides in new_images.values():
        for slide in slides:
            sample_id = slide["sample_id"]
            slide_obj = get_or_create(db.session, Slide, sample_id=sample_id)
            for z_stack in slide["z_stacks"]:
                name = z_stack["name"]
                z_stack_obj = get_or_create(
                    db.session, ZStack, slide_id=slide_obj.id, name=name
                )
                for image in z_stack["images"]:
                    file_location = image["file_location"]
                    image_obj = get_or_create(
                        db.session, Image, file_location=file_location
                    )
                    image_obj.z_stack_id = z_stack_obj.id
                    image_obj.slide_x = image["slide_x"]
                    image_obj.slide_y = image["slide_y"]
                    image_obj.slide_z = image["slide_z"]
                    try:
                        if not image_obj.check_hash():
                            image_obj.set_hash()
                    except:
                        success = False
                        failed_images.append(image_obj.file_location)
                        db.session.delete(image_obj)
                        logging.error(f"Could not set hash for {image}")

                    db.session.commit()
                if z_stack_obj.images.count() == 0:
                    db.session.delete(z_stack_obj)
                    db.session.commit()
            if slide_obj.z_stacks.count() == 0:
                db.session.delete(slide_obj)
                db.session.commit()

    return [success, failed_images]


@bp.route("/images/add-images-json", methods=["GET", "POST"])
@login_required
@admin_required
def add_images_json():
    images_json_form = AddImagesJSONForm()
    if images_json_form.validate_on_submit():
        json_file = images_json_form.json_file.data
        if json_file:
            success, failed_images = load_images_from_json(json_file.read())
            if success:
                flash("Images added.", "success")
                return redirect(url_for("admin.add_images_json"))
            else:
                flash(f"Images not added: {failed_images}", "warning")
                return redirect(url_for("admin.add_images_json"))
    return render_template(
        "admin/add-images-json.html",
        title="Add images with a JSON file",
        images_json_form=images_json_form,
    )


@bp.route("/images/add-image", methods=["GET", "POST"])
@login_required
@admin_required
def add_image():
    slides = Slide.query.all()
    z_stacks = ZStack.query.filter_by(slide_id=slides[0].id).all()
    image_form = AddImageForm()
    image_form.sample_id.choices = [
        (slide.sample_id, slide.sample_id) for slide in slides
    ]
    image_form.z_stack_id.choices = [(z_stack.id, z_stack.id) for z_stack in z_stacks]
    if image_form.validate_on_submit():
        image_file = image_form.image_file.data
        if image_file and allowed_file(image_file.filename):
            filename = secure_filename(image_file.filename)
            z_stack = ZStack.query.filter_by(id=image_form.z_stack_id.data).first()
            image_folder = Path(str(z_stack.slide.sample_id)) / str(z_stack.id)
            full_image_folder = current_app.config["SLIDES_FOLDER"] / image_folder
            if not full_image_folder.exists():
                full_image_folder.mkdir()
            image_file.save(full_image_folder / filename)
            image = Image(
                z_stack_id=image_form.z_stack_id.data,
                slide_x=image_form.slide_x.data,
                slide_y=image_form.slide_y.data,
                slide_z=image_form.slide_z.data,
                file_location=image_folder / filename,
            )
            image.set_hash()
            db.session.add(image)
            db.session.commit()
            flash("Image uploaded.", "success")
            return redirect(url_for("admin.add_image"))
    return render_template(
        "admin/add-image.html", title="Add image", image_form=image_form
    )


@bp.route("/images/add-slide", methods=["GET", "POST"])
@login_required
@admin_required
def add_slide():
    slide_form = AddSlideForm()
    if slide_form.validate_on_submit():
        slide = Slide(sample_id=slide_form.sample_id.data)
        db.session.add(slide)
        db.session.commit()
        flash("Slide added.", "success")
        return redirect(url_for("admin.add_z_stack"))
    return render_template(
        "admin/add-slide.html", title="Add slide", slide_form=slide_form
    )


@bp.route("images/z-stacks-from-sample-id", methods=["POST"])
@login_required
@admin_required
def z_stacks_from_sample_id():
    sample_id = request.form["sample_id"]
    slide = Slide.query.filter_by(sample_id=sample_id).first()
    return jsonify({z_stack.id: z_stack.id for z_stack in slide.z_stacks.all()})


@bp.route("images/add-z-stack", methods=["GET", "POST"])
@login_required
@admin_required
def add_z_stack():
    slides = Slide.query.all()
    z_stack_form = AddZStackForm()
    z_stack_form.sample_id.choices = [
        (slide.sample_id, slide.sample_id) for slide in slides
    ]
    if z_stack_form.validate_on_submit():
        slide = Slide.query.filter_by(sample_id=z_stack_form.sample_id.data).first()

        z_stack = ZStack(slide_id=slide.id)
        db.session.add(z_stack)
        db.session.commit()
        if z_stack_form.name.data == "":
            z_stack.name = str(z_stack.id)
        else:
            z_stack.name = z_stack_form.name.data
        db.session.commit()
        flash("Z stack added.", "success")
        return redirect(url_for("admin.add_image"))
    return render_template(
        "admin/add-z-stack.html", title="Add Z-stack", z_stack_form=z_stack_form
    )


@bp.route("/make-slide-active/<id>", methods=["POST"])
@login_required
@admin_required
def make_slide_active(id):
    form = EmptyForm()
    if form.validate_on_submit():
        slide = Slide.query.filter_by(id=id).first()
        if slide is None:
            flash("Slide {} not found".format(slide), "error")
            return redirect(url_for("admin.slides"))
        for z_stack in slide.z_stacks:
            z_stack.is_active = True
        db.session.commit()
        flash("Slide {} is now active".format(slide), "message")
        return redirect(url_for("admin.slides"))
    else:
        return redirect(url_for("admin.slides"))


@bp.route("/make-slide-inactive/<id>", methods=["POST"])
@login_required
@admin_required
def make_slide_inactive(id):
    form = EmptyForm()
    if form.validate_on_submit():
        slide = Slide.query.filter_by(id=id).first()
        if slide is None:
            flash("Slide {} not found".format(slide), "error")
            return redirect(url_for("admin.slides"))
        for z_stack in slide.z_stacks:
            z_stack.is_active = False
        db.session.commit()
        flash("Slide {} is now inactive".format(slide), "message")
        return redirect(url_for("admin.slides"))
    else:
        return redirect(url_for("admin.slides"))


@bp.route("/make-z-stack-active/<id>", methods=["POST"])
@login_required
@admin_required
def make_z_stack_active(id):
    form = EmptyForm()
    if form.validate_on_submit():
        z_stack = ZStack.query.filter_by(id=id).first()
        if z_stack is None:
            flash(f"Z Stack {z_stack.id} ({z_stack.name}) not found", "error")
            return redirect(url_for("admin.z_stacks"))
        if z_stack.is_active:
            flash(f"Z Stack {z_stack.id} ({z_stack.name}) is already active", "message")
            return redirect(url_for("admin.z_stacks"))
        z_stack.is_active = True
        db.session.commit()
        flash(f"Z Stack {z_stack.id} ({z_stack.name}) is now active", "message")
        return redirect(url_for("admin.z_stacks"))
    else:
        return redirect(url_for("admin.z_stacks"))


@bp.route("/make-z-stack-inactive/<id>", methods=["POST"])
@login_required
@admin_required
def make_z_stack_inactive(id):
    form = EmptyForm()
    if form.validate_on_submit():
        z_stack = ZStack.query.filter_by(id=id).first()
        if z_stack is None:
            flash(
                f"Z Stack {z_stack.id} ({z_stack.name}) not found".format(z_stack),
                "error",
            )
            return redirect(url_for("admin.z_stacks"))
        if not z_stack.is_active:
            flash(
                f"Z Stack {z_stack.id} ({z_stack.name}) is already inactive", "message"
            )
            return redirect(url_for("admin.z_stacks"))
        z_stack.is_active = False
        db.session.commit()
        flash(f"Z Stack {z_stack.id} ({z_stack.name}) is now inactive", "message")
        return redirect(url_for("admin.z_stacks"))
    else:
        return redirect(url_for("admin.z_stacks"))


@bp.route("/make-admin/<username>", methods=["POST"])
@login_required
@admin_required
def make_admin(username):
    form = EmptyForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=username).first()
        if user is None:
            flash("User {} not found.".format(username), "error")
            return redirect(url_for("admin.users"))
        if user.is_admin:
            flash("User {} is already an admin.".format(username), "message")
            return redirect(url_for("admin.users"))
        user.make_admin()
        db.session.commit()
        flash("User {} is now an admin.".format(username), "success")
        return redirect(url_for("admin.users"))
    else:
        return redirect(url_for("admin.users"))


@bp.route("/remove-admin/<username>", methods=["POST"])
@login_required
@admin_required
def remove_admin(username):
    form = EmptyForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=username).first()
        if user is None:
            flash("User {} not found.".format(username), "error")
            return redirect(url_for("admin.users"))
        if user == current_user:
            flash("You cannot remove admin from yourself.", "error")
            return redirect(url_for("admin.users"))
        if not user.is_admin:
            flash("User {} is already not an admin.".format(username), "message")
            return redirect(url_for("admin.users"))
        user.revoke_admin()
        db.session.commit()
        flash("User {} is now not an admin.".format(username), "success")
        return redirect(url_for("admin.users"))
    else:
        return redirect(url_for("admin.users"))
