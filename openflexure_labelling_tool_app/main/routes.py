from flask import render_template

from openflexure_labelling_tool_app.main import bp


@bp.route("/index")
@bp.route("/")
def index():
    return render_template("index.html", title="Home")
