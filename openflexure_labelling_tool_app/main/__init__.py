from flask import Blueprint

bp = Blueprint("main", __name__)

from openflexure_labelling_tool_app.main import routes
