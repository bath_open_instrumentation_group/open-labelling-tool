FROM python:3.8


RUN mkdir -p /deploy
WORKDIR /deploy

RUN mkdir -p instance/data

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY openflexure_labelling_tool_app openflexure_labelling_tool_app
COPY migrations migrations
COPY openflexure_labelling_tool.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP openflexure_labelling_tool.py

EXPOSE 3000
ENTRYPOINT ["./boot.sh"]
